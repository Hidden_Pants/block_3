package inheritance;

import com.IMIT.inheritance.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class СonsignmentTest {

    @Test
    public void getWeightTest() {
        IProduct[] products = new IProduct[3];
        products[0] = new PackagedPieceGoods(new PieceGoods("Chocolate","Milky",1), new ProductPackaging("SomePack",0.2),5);
        System.out.println(products[0].getGross());
        products[1] = new PackagedWeightedGoods(new WeightedGoods("Apple","Green"), new ProductPackaging("PACKAGE", 0.1),10);
        products[2] = new PackagedWeightedGoods(new WeightedGoods("POTATO", "YELLOW"), new ProductPackaging("BAG", 5),20);
        Consignment consignment = new Consignment("OREO",products);
        assertEquals(consignment.getWeight(), 40.3, 0.00001);
    }
}