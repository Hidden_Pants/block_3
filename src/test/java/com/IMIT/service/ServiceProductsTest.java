package com.IMIT.service;

import com.IMIT.inheritance.*;
import com.IMIT.interfaces.BeginStringFilter;
import org.junit.Test;
import static org.junit.Assert.*;


public class ServiceProductsTest {

    @Test
    public void countByFilter() {
        IProduct[] products = new IProduct[3];
        products[0] = new PackagedPieceGoods(new PieceGoods("Chocolate", "Milky", 1), new ProductPackaging("SomePack", 0.2), 5);
        products[1] = new PackagedWeightedGoods(new WeightedGoods("Arizona", "Green"), new ProductPackaging("PACKAGE", 0.1), 10);
        products[2] = new PackagedWeightedGoods(new WeightedGoods("Potato", "YELLOW"), new ProductPackaging("BAG", 5), 20);
        Consignment consignment = new Consignment("OREO", products);
        BeginStringFilter filter = new BeginStringFilter("Chocolate");
        ServiceProducts sp =new ServiceProducts();
        assertEquals(filter.apply(products[0].getName()), true);
        assertEquals(filter.apply(products[1].getName()), false);
        assertEquals(filter.apply(products[2].getName()), false);
        assertEquals(sp.countByFilter(filter,consignment),1);


    }


}