package com.IMIT.interfaces;

import org.junit.Test;

import static org.junit.Assert.*;

public class BeginStringFilterTest {

    @Test
    public void apply()
    {
        String str="Мама мыла раму";
        BeginStringFilter filter1=new BeginStringFilter("Мама");
        BeginStringFilter filter2=new BeginStringFilter("мыла");

        assertEquals(filter1.apply(str),true);
        assertEquals(filter2.apply(str),false);

    }
}