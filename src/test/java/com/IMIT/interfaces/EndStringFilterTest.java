package com.IMIT.interfaces;

import org.junit.Test;

import static org.junit.Assert.*;

public class EndStringFilterTest {

    @Test
    public void apply()
    {
        String str="Мама мыла раму";
        EndStringFilter filter1=new EndStringFilter("Мама");
        EndStringFilter filter2=new EndStringFilter("раму");
        boolean res1=filter1.apply(str);
        boolean res2=filter2.apply(str);
        assertEquals(filter1.apply(str),false);
        assertEquals(filter2.apply(str),true);
    }
}