package com.IMIT.inheritance;

import java.util.Objects;

public class ProductPackaging
{
    private String name;
    private double weight;

    public ProductPackaging(String nameOfProduct)
    {
        this.name = "NONAME";
        this.weight = 0;
    }
    public ProductPackaging(String name, double weight)
    {
        this.name = name;
        this.weight = weight;
    }
    public ProductPackaging(ProductPackaging obj)
    {
        name=obj.getName();
        weight=obj.getWeight();
    }

    public String getName() {
        return name;
    }

    public double getWeight() {
        return weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductPackaging that = (ProductPackaging) o;
        return Double.compare(that.weight, weight) == 0 &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, weight);
    }

    @Override
    public String toString() {
        return "ProductPackaging{" +
                "name='" + name + '\'' +
                ", weight=" + weight +
                '}';
    }
}
