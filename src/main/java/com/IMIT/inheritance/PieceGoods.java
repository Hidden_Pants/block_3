package com.IMIT.inheritance;

import java.util.Objects;

public class PieceGoods extends Product{
    private double weight;

    public PieceGoods(String name,String Description, double weight) {
        super (name,Description);
        this.weight=weight;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getNameOfProduct() {
        return super.getNameOfProduct();
    }

    public void setNameOfProduct(String nameOfProduct) {
        super.setNameOfProduct(nameOfProduct);
    }

    public String getDescription() {
        return super.getDescription();
    }

    public void setDescription(String description) {
        super.setDescription(description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PieceGoods that = (PieceGoods) o;
        return Objects.equals(super.getNameOfProduct(), that.getNameOfProduct()) &&
                Objects.equals(super.getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getNameOfProduct(),super.getDescription());
    }

    @Override
    public String toString() {
        return "PieceGoods{" +
                "NameOfProduct='" + super.getNameOfProduct() + '\'' +
                ", Description='" + super.getDescription() + '\'' +
                '}';
    }
    //price is good
}
