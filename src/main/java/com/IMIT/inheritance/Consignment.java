package com.IMIT.inheritance;

public class Consignment {
    private IProduct[] prod;
    private String Description;

    public Consignment(String Description,IProduct... products)
    {

        this.Description=Description;
        prod = products;
    }

    public double getWeight() {
        double sum = 0;
        for (int i = 0; i < prod.length; i++) {
            sum += prod[i].getGross();
        }
        return sum;
    }
    public int getSize()
    {
        return prod.length;
    }
    public IProduct getProducts(int i)
    {
        return prod[i];
    }

}
