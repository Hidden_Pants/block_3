package com.IMIT.inheritance;

import java.util.Objects;

public class PackagedPieceGoods extends PieceGoods implements IProduct {
    private int amount;
    private ProductPackaging pack;


    public PackagedPieceGoods(PieceGoods piece, ProductPackaging pack, int amount) {
        super(piece.getNameOfProduct(),piece.getDescription(),piece.getWeight());
        this.pack=new ProductPackaging(pack);
        this.amount = amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public ProductPackaging getPack() {
        return pack;
    }

    public void setPack(ProductPackaging pack) {
        this.pack.setName(pack.getName());
        this.pack.setWeight(pack.getWeight());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PackagedPieceGoods that = (PackagedPieceGoods) o;
        return amount == that.amount &&
                Objects.equals(pack, that.pack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), amount, pack);
    }

    @Override
    public String toString() {
        return "PackagedPieceGoods{" +
                "amount=" + amount +
                ", pack=" + pack +
                '}';
    }

    @Override
    public double getNet() {
        return amount*getWeight();
    }

    @Override
    public double getGross() {
        return this.amount*this.getWeight()+pack.getWeight();
    }

    @Override
    public String getName()
    {
        return this.getNameOfProduct();
    }

    @Override 
    public String getDescription()
    {
        return this.getDescription();
    }

    @Override
    public boolean IsWeighted()
    {
        return false;
    }
}
