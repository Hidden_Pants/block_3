package com.IMIT.inheritance;

public interface IProduct {
    double getNet();

    double getGross();

    String getName();

    String getDescription();

    boolean IsWeighted();

    static int fifth()
    {
        return 5;
    }


}
