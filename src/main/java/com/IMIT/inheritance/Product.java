package com.IMIT.inheritance;

public class Product {
    private String NameOfProduct;
    private String Description;

    public Product(String nameOfProduct, String description) {
        NameOfProduct = nameOfProduct;
        Description = description;
    }

    public String getNameOfProduct() {
        return NameOfProduct;
    }

    public void setNameOfProduct(String nameOfProduct) {
        NameOfProduct = nameOfProduct;
    }

    public String getDescription()
    {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    @Override
    public String toString() {
        return "Product{" +
                "NameOfProduct='" + NameOfProduct + '\'' +
                ", Description='" + Description + '\'' +
                '}';
    }
}
