package com.IMIT.inheritance;

import java.util.Objects;

public class WeightedGoods extends Product{

    public WeightedGoods(String name, String Description) {
        super(name, Description);
    }

    public WeightedGoods(Product prod) {
        super(prod.getNameOfProduct(),prod.getDescription());
    }

    public String getNameOfProduct() {
        return super.getNameOfProduct();
    }

    public void setNameOfProduct(String nameOfProduct) {
        super.setNameOfProduct(nameOfProduct);
    }

    public String getDescription() {
        return super.getDescription();
    }

    public void setDescription(String description) {
        super.setDescription(description);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WeightedGoods that = (WeightedGoods) o;
        return Objects.equals(super.getNameOfProduct(), that.getNameOfProduct()) &&
                Objects.equals(super.getDescription(), that.getDescription());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.getNameOfProduct(),super.getDescription());
    }

    @Override
    public String toString() {
        return "WeightedGoods{" +
                "NameOfProduct='" + super.getNameOfProduct() + '\'' +
                ", Description='" + super.getDescription() + '\'' +
                '}';
    }
}
