package com.IMIT.inheritance;

import java.util.Objects;

public class PackagedWeightedGoods extends WeightedGoods implements IProduct{
    private ProductPackaging pack;
    private double weight;

    public PackagedWeightedGoods(Product prod, ProductPackaging pack,double weight) {
        super(prod);
        this.pack = new ProductPackaging(pack);
        this.weight=weight;

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PackagedWeightedGoods that = (PackagedWeightedGoods) o;
        return Double.compare(that.weight, weight) == 0 &&
                Objects.equals(pack, that.pack);
    }

    @Override
    public int hashCode() {
        return Objects.hash(pack, weight);
    }

    @Override
    public String toString() {
        return "PackagedWeightedGoods{" +
                "pack=" + pack +
                ", weight=" + weight +
                '}';
    }

    @Override
    public double getNet() {
        return weight;
    }

    @Override
    public double getGross() {
        return weight+pack.getWeight();
    }

    @Override
    public String getName()
    {
        return this.getNameOfProduct();
    }

    @Override
    public String getDescription()
    {
        return this.getDescription();
    }

    @Override
    public boolean IsWeighted()
    {
        return true;
    }
}
