package com.IMIT.service;

import com.IMIT.interfaces.Filter;

import com.IMIT.inheritance.Consignment;

public class ServiceProducts {
    public int countByFilter(Filter filter, Consignment consignment) {
        int count = 0;
        for (int i = 0; i < consignment.getSize(); i++) {
            if (filter.apply(consignment.getProducts(i).getName())) {
                count++;
            }
        }
        return count;
    }

    /*public int countByFilterDeep(Filter filter, Consignment consignment,String str) {
        int count = 0;
        for (int i = 0; i < consignment.getSize(); i++) {
            if (filter.apply(consignment.getProducts(i).getName()) || filter.apply(str)) {
                count++;
            }
        }
        return count;
    }*/


}
