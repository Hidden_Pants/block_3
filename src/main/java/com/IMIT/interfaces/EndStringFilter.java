package com.IMIT.interfaces;

public class EndStringFilter implements Filter{
    private String pattern;

    public EndStringFilter(String string){
        pattern = string;
    }

    public boolean apply(String string){
        return string.endsWith(pattern);
    }
}