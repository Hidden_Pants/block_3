package com.IMIT.interfaces;

public class BeginStringFilter implements Filter {
    private String pattern;

    public BeginStringFilter(String pattern) {
        this.pattern = pattern;
    }

    public boolean apply(String string) {
        return string.startsWith(pattern);
    }
}
