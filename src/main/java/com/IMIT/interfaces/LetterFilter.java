package com.IMIT.interfaces;

public class LetterFilter implements Filter {

    public boolean apply(String letter) {
        return letter.charAt(0) == 'М';
    }
}
